#include "./fvector.hh"
#include <boost/python.hpp>
#include <boost/python/docstring_options.hpp>

BOOST_PYTHON_MODULE(libfvector)
{
    using namespace boost::python;

    docstring_options doc_options(true);

    WrapFieldVector<double, 1>();
    WrapFieldVector<double, 2>();
    WrapFieldVector<double, 3>();
}

import importlib.util

spec = importlib.util.spec_from_file_location("libfvector", "../../build-cmake/src/libfvector.so")
libfvector = importlib.util.module_from_spec(spec)
spec.loader.exec_module(libfvector)

import unittest

class TestWrapperDoubleFieldVector(unittest.TestCase):

    def test_constructorByValue(self):
        x = libfvector.FieldVector1(3.14)
        self.assertEqual(x[0], 3.14)

        y = libfvector.FieldVector3(1.1)
        for i in range(0, 3):
            self.assertEqual(y[i], 1.1)

    def test_constructorByCopy(self):
        x = libfvector.FieldVector3(3.14)
        y = libfvector.FieldVector3(x)

        for i in range(0, 3):
            self.assertEqual(y[i], x[i])

    def test_getItem(self):
        x = libfvector.FieldVector1(3.14)
        self.assertEqual(x[0], 3.14)

    def test_len(self):
        x = libfvector.FieldVector1(1)
        y = libfvector.FieldVector2(2)
        z = libfvector.FieldVector3(3)

        self.assertEqual(len(x), 1)
        self.assertEqual(len(y), 2)
        self.assertEqual(len(z), 3)

if __name__ == '__main__':
    unittest.main()

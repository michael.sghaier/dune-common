#include <algorithm>

#include <../dune/common/fvector.hh>

#include <boost/python.hpp>
#include <boost/shared_ptr.hpp>

using namespace boost::python;

template<class K, int SIZE>
struct WrapFieldVector
{
    typedef Dune::FieldVector<K, SIZE> FVector;

    static K& get_item(FVector* fvector, unsigned int i)
    {
        if (i >= SIZE)
            throw std::out_of_range("FieldVector of size " +
                    std::to_string(SIZE) + " ; " +
                    std::to_string(i) + " >= " + std::to_string(SIZE));

        return (*fvector)[i];
    }

    static void set_item(FVector* fvector, unsigned int i, K x)
    {
        if (i >= SIZE)
            throw std::out_of_range("FieldVector of size " +
                    std::to_string(SIZE) + " ; " +
                    std::to_string(i) + " >= " + std::to_string(SIZE));

        (*fvector)[i] = x;
    }

    static std::shared_ptr<FVector> from_list(object& list)
    {
        stl_input_iterator<K> begin(list), end;

        auto fvector = std::make_shared<FVector>();

        std::copy(begin, end, fvector->begin());

        return fvector;
    }


    WrapFieldVector()
    {
        class_<FVector>(("FieldVector" + std::to_string(SIZE)).c_str())
            .def(init<>("Constructor making default-initialized vector"))

            .def(init<K>(args("t"),
                        "Constructor making vector with identical coordinates"))

            .def(init<const FVector>(args("x"),
                        "Copy constructor"))

            .def("__init__", make_constructor(from_list))

            .def("__len__", &FVector::size, "Return the length of the given field vector")

            .def("__getitem__", get_item, return_value_policy<return_by_value>(),
                    args("i"), "Get the ith item of the given field vector")

            .def("__setitem__", set_item)
        ;
    }

};

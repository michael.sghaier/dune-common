#include "./fmatrix.hh"
#include <boost/python.hpp>
#include <boost/python/docstring_options.hpp>

BOOST_PYTHON_MODULE(libfmatrix)
{
    using namespace boost::python;

    docstring_options doc_options(true);

    WrapFieldMatrix<double, 1, 1>();
    WrapFieldMatrix<double, 1, 2>();
}

#include <../dune/common/fmatrix.hh>
#include <../dune/common/fvector.hh>
#include <boost/python.hpp>

using namespace boost::python;

template<class K, int ROWS, int COLS>
class WrapFieldMatrix
{
public:
    typedef typename Dune::FieldMatrix<K, ROWS, COLS> FMatrix;

    static FMatrix leftmultiplyany(FMatrix* fmatrix, object const& M)
    {
        Dune::FieldMatrix<K, ROWS, ROWS>& fmatrix2 =
            extract<Dune::FieldMatrix<K, ROWS, ROWS>&>(M);

        return fmatrix->leftmultiplyany(fmatrix2);
    }

    static FMatrix rightmultiplyany(FMatrix* fmatrix, object const& M)
    {
        Dune::FieldMatrix<K, COLS, COLS>& fmatrix2 =
            extract<Dune::FieldMatrix<K, COLS, COLS>&>(M);

        return fmatrix->rightmultiplyany(fmatrix2);
    }

    static FMatrix& rightmultiply(FMatrix* fmatrix, object const& M)
    {
        Dune::FieldMatrix<K, COLS, COLS>& fmatrix2 =
            extract<Dune::FieldMatrix<K, COLS, COLS>&>(M);

        return fmatrix->rightmultiply(fmatrix2);
    }

    static int mat_rows(FMatrix* _)
    {
        return ROWS;
    }

    static int mat_cols(FMatrix* _)
    {
        return COLS;
    }

    static Dune::FieldVector<K, COLS>& mat_access(FMatrix* fmatrix, unsigned int i)
    {
        return fmatrix->mat_access(i);
    }

    WrapFieldMatrix()
    {
        class_<FMatrix>(("FieldMatrix" +
                         std::to_string(ROWS) + std::to_string(COLS)).c_str())

            .def(init<>("Default constructor"))

            .def(init<K>(args("t"),
                        "Constructor initializing the whole matrix with a scalar"))

            .def("leftmultiplyany", leftmultiplyany, return_value_policy<return_by_value>(),
                    "Multiplies M from the left to this matrix, this matrix is not modified")

            .def("rightmultiply", rightmultiply, return_value_policy<reference_existing_object>(),
                    "Multiplies M from the right to this matrix")

            .def("rightmultiplyany", rightmultiplyany, return_value_policy<return_by_value>(),
                    "Multiplies M from the right to this matrix, this matrix is not modified")

            .def("mat_rows", mat_rows, "Return the number of rows of this matrix")

            .def("mat_cols", mat_cols, "Return the number of columns of this matrix")

            .def("mat_access", mat_access, return_value_policy<reference_existing_object>(),
                    args("i"), "Return the ith row (a FieldVector) of this matrix");
    }
};
